/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
(function webpackMissingModule() { throw new Error("Cannot find module \"server\""); }());


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tirage__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tirage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__tirage__);


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// const array = ["Aline", "Jean-luc", "Robert", "Paulette", "Charlette", "Francois", "Margaux"];

///////////////////////////CODE//////////////////////////////
const form = document.getElementById('form');
const array = [];

form.addEventListener("submit", (event) =>{
 	event.preventDefault();
 	const names = document.getElementById('inputnames').value;

 	const liste = document.getElementById('liste');
 	liste.insertAdjacentHTML('afterend', `<li>${names}</li>`)
 	array.push(names);
});


const button = document.getElementById('melange');

 button.addEventListener("click", (event) =>{
 	event.preventDefault();

 	if (array.length % 2 != 0) {
 		const cadeau = document.getElementById('cadeau');
 		cadeau.insertAdjacentHTML('afterend', `<li>Le nombre de personne n'est pas pair. Actuellement il n'y que ${array.length} prénoms</li>`)
 	} else {
 	    var arr1 = array.slice(), // copy array
 	        arr2 = array.slice(); // copy array again

 	    arr1.sort(function() { return 0.5 - Math.random();}); // shuffle arrays
 	    arr2.sort(function() { return 0.5 - Math.random();});

 	    while (arr1.length) {
 	        var name1 = arr1.pop(), // get the last value of arr1
 	            name2 = arr2[0] == name1 ? arr2.pop() : arr2.shift();
 	            //        ^^ if the first value is the same as name1, 
 	            //           get the last value, otherwise get the first

 	        cadeau.insertAdjacentHTML('afterend', `<li>${name1} offre un cadeau à ${name2}</li>`)
 	    }
 	}
 	
});

/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map